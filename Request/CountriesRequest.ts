import Axios from "axios";
import CountryData from "../Models/CountryData";

export default class CountriesRequest {
    async getCountriesData() {
        let countryData = [];
        await Axios.get('https://corona.lmao.ninja/countries')
            .then(function (response) {
                for (const value of response.data) {
                    countryData.push(new CountryData(value.country, value.cases, value.todayCases, value.deaths, value.todayDeaths, value.recovered, value.critical));
                }
            })
            .catch(function (error) {
                console.log(error);
            });

        return countryData;
    }
}
