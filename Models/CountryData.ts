export default class CountryData {
    private _country;
    private _cases;
    private _todayCases;
    private _deaths;
    private _todayDeaths;
    private _recovered;
    private _critical;

    constructor(country: string, cases: number, todayCases: number, deaths: number, todayDeaths: number, recovered: number, critical: number) {
        this._country = country;
        this._cases = cases;
        this._todayCases = todayCases;
        this._deaths = deaths;
        this._todayDeaths = todayDeaths;
        this._recovered = recovered;
        this._critical = critical;
    }

    get country() {
        return this._country;
    }

    get cases() {
        return this._cases;
    }

    get todayCases() {
        return this._todayCases;
    }

    get deaths() {
        return this._deaths;
    }

    get todayDeaths() {
        return this._todayDeaths;
    }

    get recovered() {
        return this._recovered;
    }

    get critical() {
        return this._critical;
    }
}
