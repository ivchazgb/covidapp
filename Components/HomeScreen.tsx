import React from 'react';
import {Component} from "react";
import {StyleSheet} from "react-native";
import {Layout, List, ListItem, Spinner, Input} from "@ui-kitten/components";
import Axios from "axios";
import CountryData from "../Models/CountryData";

export default class HomeScreen extends Component {

    private navigation;

    state = {
        dataPerCountry: [],
        spinner: true,
        allCountries: []
    };

    constructor(navigation, props) {
        super(props);
        this.navigation = navigation;
    }

    filterCountries(text: string) {
        console.log(text);
        if (text === ''){
            this.setState({
                dataPerCountry: this.state.allCountries
            })
        }
        this.state.dataPerCountry.forEach((name)=> {
            console.log(name);
            if (name.country === text){
                console.log('evo ga');
                this.setState({
                    dataPerCountry: [name]
                })
            }
        });
    }

    componentDidMount(): void {
        this.getCountriesData();
    }

    render() {
        let renderItem = ({item}) => (
            <ListItem title={item.country} onPress={() => {
                this.navigation.navigate('Country', {country: item});
            }}/>
        );

        const spinner = <Spinner status='basic'/>;

        const input = <Input placeholder='Search' onChangeText={text => this.filterCountries(text)}/>;
        return (
            <Layout style={styles.listings}>
                {this.state.spinner ? spinner : input}
                <List data={this.state.dataPerCountry} renderItem={renderItem}/>
            </Layout>
        );
    }

    async getCountriesData() {
        console.log('ispocetka');
        let countryData = [];
        await Axios.get('https://corona.lmao.ninja/countries')
            .then(function (response) {
                for (const value of response.data) {
                    countryData.push(new CountryData(value.country, value.cases, value.todayCases, value.deaths, value.todayDeaths, value.recovered, value.critical));
                }
            })
            .catch(function (error) {
                console.log(error);
            });

        this.setState({
            dataPerCountry: countryData,
            spinner: false,
            allCountries: countryData
        })
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    listings: {
        flex: 1,
        paddingTop: 10
    },
});
