import * as React from 'react';
import {View, StyleSheet} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  ApplicationProvider,
} from '@ui-kitten/components';
import {
  mapping,
  dark as theme,
} from '@eva-design/eva';
import {Component} from "react";
import HomeScreen from "./Components/HomeScreen";
import CountryScreen from "./Components/CountryScreen";

export default class App extends Component {
  public Stack = createStackNavigator();
  render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
    return (
        <ApplicationProvider mapping={mapping} theme={theme}>
          <NavigationContainer>
            <this.Stack.Navigator initialRouteName="Home">
              <this.Stack.Screen name="Home" component={({navigation}) => <HomeScreen { ...navigation }/>}/>
              <this.Stack.Screen name="Country" component={(navigation)=> <CountryScreen { ...navigation }/>}/>
            </this.Stack.Navigator>
          </NavigationContainer>
        </ApplicationProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
